const person = {
	name: "Foobar",
	address: "Jakarta",
	meta: {
		invoices: [
			{ number: "INV08633", total: 89000 },
			{ number: "INV08634", total: 74000 }
    ]
  }
}

const { name, address, meta } = person
