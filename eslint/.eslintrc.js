module.exports = {
  root: true,
  parserOptions: { parser: 'babel-eslint' },
  env: { browser: true },
  rules: {
    // 'padded-blocks': ["error", { "classes": "always" }]
  },
  extends: [
    'standard'
  ],
}
