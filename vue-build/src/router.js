import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const Router = [
  { path: '/', component: () => import('./views/Todo.vue'), meta: { requiredAuth: true } },
  { path: '/login', component: () => import('./views/Login.vue') },
  {
    path: '/product/:id',
    component: () => import('./views/ProductView.vue'),
    children: [
      { path: '', component: () => import('./views/ProductDescription.vue') },
      { path: 'spect', component: () => import('./views/ProductSpect.vue') }
    ]
  }
]

const Routes = new VueRouter({
  mode: 'history',
  routes: Router
})

Routes.beforeEach((to, from, next) => {
  if (to.matched.some(route => route.meta.requiredAuth === true)) {
    if (window.localStorage.getItem('auth')) {
      next()
    } else {
      next({ path: '/login' })
    }
  } else {
    next()
  }
})

export default Routes
