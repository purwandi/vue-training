import http from '../services/http'
import Nprogress from 'nprogress'

export default {
  namespaced: true,
  state: {
    todos: [],
    todo: {}
  },

  getters: {
    getTodos: state => state.todos,
    getTodosById: (state) => (id) => {
      return state.todos.find(item => item.id === id)
    },
    getNotDoneTodos: state => state.todos.filter(item => item.done === false),
    doneTodos: state => state.todos.filter(item => item.done === true),
    doneTodosLength: (state, getters) => {
      return getters.doneTodos.length
    }
  },

  mutations: {
    addTodo (state, payload) {
      state.todos.push(payload)
    },
    receivedTodo (state, payload) {
      state.todos = payload
    },
    setCurrentTodo (state, payload) {
      state.todo = payload
    }
  },

  actions: {
    fetchApi ({ state, commit }) {
      http.get('todos')
        .then(response => {
          Nprogress.done()
          commit('receivedTodo', response)
        })
        .catch(error => {
          debugger
        })
    },

    postTodo ({ state, commit }, payload) {
      fetch('http://localhost:4000/todos', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
      })
        .then(response => response.json())
        .then(response => {
          commit('addTodo', response)
        })
    }
  }
}
