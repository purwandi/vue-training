import NProgress from 'nprogress'
import axios from 'axios'

const isBrowser = typeof window !== 'undefined'

const url = process.env.NODE_ENV === 'production'
  ? 'http://localhost:4000/'
  : 'http://localhost:4000/'

const http = {

  request (method, path, params) {
    if (isBrowser) NProgress.start()

    return axios({
      method,
      url: url + path,
      data: params,
      headers: {
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
  },

  post (url, params) {
    return this.request('POST', url, params)
  },

  get (url, params) {
    return this.request('GET', url, params)
  },

  put (url, params) {
    return this.request('PUT', url, params)
  },

  delete (url, params) {
    return this.request('DELETE', url, params)
  },

  init () {
    // Intercept the request to make sure the token is injected into the header.
    axios.interceptors.request.use(config => {
      // we intercept axios request and add authorizatio header before perform send a request to the server
      // config.headers.Authorization = `Bearer ${ls.get('jwt-token')}`
      return config
    })

    // Intercept the response and…
    axios.interceptors.response.use(response => {
      if (isBrowser) NProgress.done()

      // if (isBrowser) {
      // …get the token from the header or response data if exists, and save it.
      const token = response.headers['Authorization'] || response.data['token']
      if (isBrowser) window.localStorage.setItem('auth-token', token)
      // }

      return response
    }, error => {
      if (isBrowser) NProgress.done()
      // Also, if we receive a Bad Request / Unauthorized error
      if (error.response.status === 400 || error.response.status === 401) {
        // and we're not trying to login
      }

      return Promise.reject(error)
    })
  }

}

export default http
