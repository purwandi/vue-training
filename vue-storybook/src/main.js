import Vue from 'vue'
import App from './App.vue'

import router from './router'
import http from './services/http'
import store from './store/index'

import './scss/app.scss'
import './registerServiceWorker'

Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App),
  created () {
    http.init()
  }
}).$mount('#app')
