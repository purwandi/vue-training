import http from '../services/http'

export default {
  namespaced: true,
  state: {
    todos: [],
    todo: {}
  },

  getters: {
    getTodos: state => state.todos,
    getTodosById: (state) => (id) => {
      return state.todos.find(item => item.id === id)
    },
    getNotDoneTodos: state => state.todos.filter(item => item.done === false),
    doneTodos: state => state.todos.filter(item => item.done === true),
    doneTodosLength: (state, getters) => {
      return getters.doneTodos.length
    }
  },

  mutations: {
    addTodo (state, payload) {
      state.todos.push(payload)
    },
    removeTodo (state, payload) {
      let index = state.todos.findIndex(item => item.id === payload.id)
      state.todos.splice(index, 1)
    },
    receivedTodo (state, payload) {
      state.todos = payload
    },
    setCurrentTodo (state, payload) {
      state.todo = payload
    },
    updateTodo (state, payload) {
      let index = state.todos.findIndex(item => item.id === payload.id)
      if (index !== -1) state.todos.splice(index, 1, payload)
    }
  },

  actions: {
    fetchApi ({ state, commit }) {
      http.get('todos')
        .then(({ data }) => {
          commit('receivedTodo', data)
        })
    },

    updateTodo ({ state, commit }, payload) {
      const { id } = payload
      http.put('todos/' + id)
        .then(({ data }) => {
          commit('updateTodo', data)
        })
    },

    postTodo ({ state, commit }, payload) {
      http.post('todos', payload)
        .then(({ data }) => {
          commit('addTodo', data)
        })
    },

    removeTodo ({ state, commit }, payload) {
      const { id } = payload
      http.delete('todos/' + id)
        .then(() => {
          commit('removeTodo', payload)
        })
    }
  }
}
