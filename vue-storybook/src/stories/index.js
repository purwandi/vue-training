import { storiesOf } from '@storybook/vue'
import Cart from './../components/Cart.vue'

storiesOf('Cart', module)
  .add('Menampilkan list shopping cart', () => ({
    data () {
      return {
        list: [
          { id: 2, name: 'Aqua', done: true }
        ]
      }
    },
    components: { Cart },
    template: '<cart :cart="list" />'
  }))
  .add('Menampilkan pesan error apabila cart masih kosong', () => ({
    data () {
      return {
        list: []
      }
    },
    components: { Cart },
    template: '<cart :cart="list" />'
  }))

  .add('error delete yyn', () => ({
    data () {
      return {
        list: [
          { id: 2, name: 'Aqua', done: true },
          { id: 3, name: 'Aqua', done: true },
          { id: 4, name: 'Aqua', done: true }

        ],
        error: { error: false, message: 'errror' }
      }
    },
    methods: {
      deleted () {
        this.error = { error: true, message: 'hahaha' }
      }
    },
    components: { Cart },
    template: `
        <div>
          <h1 v-if="error.error">{{error.message}}</h1>
          <cart :onDelete="deleted" :cart="list" />
        </div>
       `
  }))
