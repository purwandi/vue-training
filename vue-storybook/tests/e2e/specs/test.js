// https://docs.cypress.io/api/introduction/api.html

describe('My First Test', () => {
  it('Visits the app root url', () => {
    cy.visit('/')
    cy.contains('a', 'Todo')
    cy.contains('a', 'Login')
  })

  it('Can fill username field', () => {
    cy.visit('/login')
    cy.get('input').type('Foobar').should('have.value', 'Foobar')
    cy.get('button').click()
    cy.location().should(location => {
      expect(location.pathname).to.eq("/")
    })
  })
})
