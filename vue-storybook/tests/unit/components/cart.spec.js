import { mount } from '@vue/test-utils'
import { expect } from 'chai'
import Cart from './../../../src/components/Cart.vue'

describe('Cart component test suite', () => {
  it('can render product cart list', () => {
    const wrapper = mount(Cart, {
      propsData: {
        cart: [
          { id: 1, name: 'Sepatu baru', done: true },
          { id: 2, name: 'Sepatu baru', done: true }
        ],
        onDelete () {}
      }
    })
    expect(wrapper.text()).to.include('Cart')
  })

  it('display error message when cart is empty', () => {
    const wrapper = mount(Cart, {
      propsData: {
        cart: [],
        onDelete () {}
      }
    })
    expect(wrapper.text()).to.include('Cart is empty')
  })
})
