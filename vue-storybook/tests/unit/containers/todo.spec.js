import { mount, createLocalVue } from '@vue/test-utils'
import { expect } from 'chai'
import Vuex from 'vuex'
import TodoContainer from './../../../src/containers/TodoContainer'

const localVue = createLocalVue()
localVue.use(Vuex)

const todo = {
  namespaced: true,
  state: {
    todos: [
      { id: 1, text: 'Hello aqua', done: true },
      { id: 2, text: 'Hello banjir', done: false }
    ]
  },
  getters: {
    getTodos: state => state.todos
  },
  actions: {
    postTodo ({ state, commit }, payload) {

    },
    removeTodo ({ state, commit }, payload) {
      return Promise.resolve()
    }
  }
}

describe('Todo container test suite', () => {
  it('can remove todo if todo is done', () => {
    // create vuex mock
    const store = new Vuex.Store({ modules: { todo } })
    const wrapper = mount(TodoContainer, {
      localVue,
      mocks: { $store: store }
    })

    const result = !!wrapper.vm.onRemove({ id: 1, text: 'Hello aqua', done: true })
    expect(result).to.be.true
  })

  it('can not remove todo if the todo is not done', async () => {
    // create vuex mock
    const store = new Vuex.Store({ modules: { todo } })
    const wrapper = mount(TodoContainer, {
      localVue,
      mocks: { $store: store }
    })

    const result = !!wrapper.vm.onRemove({ id: 2, text: 'Hello banjir', done: true })
    expect(result).to.be.true
  })
})
