import { expect } from 'chai'
import todo from './../../../src/store/todo'
import http from './../../../src/services/http'
import sinon from 'sinon'

// import todo vuex state
const { addTodo } = todo.mutations
const { getTodos, getTodosById } = todo.getters
const { fetchApi } = todo.actions

describe('Todo vuex store mutation test suite', () => {
  it('can add new todo', () => {
    // mock initial state
    const state = { todos: [] }
    // apply mutations
    addTodo(state, { id: 1, text: 'Beli aqua', done: true })
    // assert result
    expect(state.todos.length).to.equal(1)
  })
})

describe('Todo vuex store getter test suite', () => {
  // mock initial state
  const state = {
    todos: [
      { id: 1, text: 'Beli aqua', done: true }
    ]
  }
  it('can get all todos', () => {
    const result = getTodos(state)
    expect(result).to.deep.equal([
      { id: 1, text: 'Beli aqua', done: true }
    ])
  })

  it('can find todo by id', () => {
    const result = getTodosById(state)(1)
    expect(result).to.deep.equal(
      { id: 1, text: 'Beli aqua', done: true }
    )
  })
})

describe('Todo vuex store actions test suite', () => {
  // mock initial state
  const state = {
    todos: []
  }

  // Harus menggunakan async function karena di action ini
  // memanggil http service yang menggunakan promise
  it('can fetch todos api', async () => {
    const commit = sinon.stub()

    // Mocking http service
    // karea pada fetchApi action pada saat menerima response kita
    // mengambil ({ data }) maka disini juga kita melakukan resolving
    sinon.stub(http, 'get').resolves({
      data: [
        { id: 1, text: 'Beli aqua', done: true },
        { id: 2, text: 'Beli jeruk', done: false }
      ]
    })

    // waiting the response
    await fetchApi({ commit, state })

    // catch the commit args after the response is success
    expect(commit.args).to.deep.equal([
      ['receivedTodo', [
        { id: 1, text: 'Beli aqua', done: true },
        { id: 2, text: 'Beli jeruk', done: false }
      ]]
    ])
  })
})
