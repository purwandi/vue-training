import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const Route = [
  { path: '', component: () => import('./views/Home.vue') },
  { path: '/product', name: 'ProductPage', component: () => import('./views/Product.vue') }
]


const Router = new VueRouter ({
  routes: Route
})

export default Router
