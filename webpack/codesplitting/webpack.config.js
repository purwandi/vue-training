const path = require('path')

module.exports = {
  entry: {
    entry1: './src/entry1.js',
    entry2: './src/entry2.js'
  },
  mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].bundle.js'
  },
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  }
}
