const path = require('path')

module.exports = {
  entry: './src/index.js',
  mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: "",
    filename: 'js/bundle.js'
  },
  module: {
    rules: [
      // {
      //   test: /\.(png|jpp|jpeg)$/,
      //   use: [
      //     {
      //       loader: 'file-loader',
      //       options: {
      //         name: '[path][name].[ext]',
      //         emitFile: true,
      //         useRelativePath: true
      //       }
      //     }
      //   ]
      // },
      {
        test: /\.scss$/,
        use: [
          "style-loader", // creates style nodes from JS strings
          "css-loader", // translates CSS into CommonJS
          "sass-loader" // compiles Sass to CSS, using Node Sass by default
        ]
      },
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          query: {}
        }
      }
    ]
  }
}
